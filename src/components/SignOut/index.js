import React from 'react';
import {Button} from 'react-bootstrap';
import {withFirebase} from '../Firebase'
import {withRouter} from 'react-router-dom'

import './index.css'
import * as ROUTES from '../../constants/routes';


const SignOutButton = (props) => {
    const rt = props.history;
    const fb = props.firebase;

    const handleClick = () => {
        fb.signOut().then(res => {
            rt.push(ROUTES.LANDING);
        })
    }
    
    return(
        <Button variant="submit" onClick={handleClick}>
            <span id="buttonText">Signout</span>
        </Button> 
    )
}

export default withRouter(withFirebase(SignOutButton));