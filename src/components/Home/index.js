import React from 'react';
import {withSession} from '../Session'

import './index.css'

const Home = (props) => {
    return(
        <div id="homeContainer">
            <h1>Current user:</h1>
            <h2>{props.userState.email}</h2>
        </div>
    )
}

export default withSession(Home);