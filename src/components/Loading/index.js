import React from 'react'
import { Ring } from "react-awesome-spinners";
import './index.css'


const LoadingDisplay = () => {
    return(
        
    <div id="center">
        <Ring id="center" color="#222222" size="150" />
    </div>


    )
}

export default LoadingDisplay