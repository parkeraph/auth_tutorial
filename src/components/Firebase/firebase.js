import app from "firebase";
import 'firebase/auth'

const config = {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: "",
    messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_APP_ID
  };

  class Firebase {
      constructor(){
          app.initializeApp(config)
          this.auth = app.auth();
      }

      createUserWithEmailAndPassword = (email, password) => {
         return this.auth.createUserWithEmailAndPassword(email, password);
      }

      signInWithEmailAndPassword = (email, password) => {
         return this.auth.signInWithEmailAndPassword(email, password);
      }

      signOut = () => {
        return this.auth.signOut();
        console.log("signed out")
      }
      doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

      doPasswordUpdate = password => {
        return this.auth.currentUser.updatePassword(password);
      }
  }

  export default Firebase;